package ru.t1.volkova.tm.api.repository;

import ru.t1.volkova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
